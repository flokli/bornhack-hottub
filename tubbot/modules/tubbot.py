import sopel
import paho.mqtt.client as mqtt
from sopel.config.types import StaticSection, ValidatedAttribute
import threading
import time

bot = None


class TubbotSection(StaticSection):
    hot_threshold = ValidatedAttribute('hot_threshold', float, default=37)
    cold_threshold = ValidatedAttribute('cold_threshold', float, default=34)
    mqtt_server = ValidatedAttribute('mqtt_server', str)
    mqtt_topic = ValidatedAttribute('mqtt_topic', str)
    public_channel = ValidatedAttribute('public_channel', str)
    public_message = ValidatedAttribute('public_message', str)
    private_channel = ValidatedAttribute('private_channel', str)
    private_message = ValidatedAttribute('private_message', str)


def on_mqtt_connect(client, userdata, flags, result):
    client.subscribe(bot.config.tubbot.mqtt_topic)


def on_mqtt_message(client, userdata, msg):
    previous_temp = bot.memory['current_temp']
    bot.memory['current_temp'] = float(msg.payload.decode('utf-8'))

    print("received mqtt message, current temp is {}, previus temp is {}".format(bot.memory['current_temp'], previous_temp))
    print("hot_threshold: {}, cold_threshold: {}")

    if previous_temp < bot.config.tubbot.hot_threshold and bot.memory['current_temp'] >= bot.config.tubbot.hot_threshold:
        bot.say(bot.config.tubbot.public_message, bot.config.tubbot.public_channel)
    if previous_temp > bot.config.tubbot.cold_threshold and bot.memory['current_temp'] < bot.config.tubbot.cold_threshold:
        bot.say(bot.config.tubbot.private_message, bot.config.tubbot.private_channel)


def mqtt_main():
    mqttc = mqtt.Client()
    mqttc.on_connect = on_mqtt_connect
    mqttc.on_message = on_mqtt_message

    time.sleep(15)
    mqttc.connect(bot.config.tubbot.mqtt_server)
    # TODO: UGLY, let's wait a bit to ensure we joined the channel
    mqttc.loop_start()


def setup(b):
    global bot
    bot = b
    bot.config.define_section('tubbot', TubbotSection)
    bot.memory['current_temp'] = 0

    # create a separate mqtt thread
    mqtt_thread = threading.Thread(target=mqtt_main)
    mqtt_thread.daemon = True
    mqtt_thread.start()


@sopel.module.commands('t', 'temp', 'temperature')
def status(bot, trigger):
    if bot.memory['current_temp'] == 0:
        bot.say('Dunno ¯\_(ツ)_/¯')
    else:
        bot.say('Current Hot Tub Temperature: {:f}'.format(bot.memory['current_temp']))
