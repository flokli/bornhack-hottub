#!/usr/bin/env nix-shell
#!nix-shell --pure -i python3 -p "python3.withPackages (ps: with ps; [ requests paho-mqtt ])"

import threading
import requests
import paho.mqtt.client as mqtt

mqttc = mqtt.Client()
mqttc.connect("test.mosquitto.org")
mqttc.loop_start()


def update_metrics():
    # call ourselves again in 5secs
    threading.Timer(20, update_metrics).start()

    # request temperature from the esp
    r = requests.get("http://151.216.11.84/data")

    # this also returns a list of historic records.
    # we only care about the last one
    data = r.json()
    current_temp = data[-1]

    # sometimes, the temperature is 0. Ignore this
    if current_temp == 0:
        return

    print(f"current_temperature: {current_temp} °C")
    mqttc.publish("/dk/bornhack/hottub/water_temp", current_temp, retain=True)


update_metrics()
